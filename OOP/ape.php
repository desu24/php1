<?php
require_once('animal.php');
class ape extends animal
{
    public $names = "Kera Sakti";
    public $legs = 2;

    public function __construct($kera)
    {
        $this->yell = $kera;
    }
}
