<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("Shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br> <br>"; // "no"


$kodok = new frog("Hop Hop");



echo "Name : " . $kodok->nama . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : " . $kodok->jump . "<br> <br>";

$monkey = new ape("Auoooo");

echo "Name : " . $monkey->names . "<br>";
echo "Legs : " . $monkey->legs . "<br>";
echo "Cold Blooded : " . $monkey->cold_blooded . "<br>";
echo "Yell : " . $monkey->yell . "<br> <br>";
